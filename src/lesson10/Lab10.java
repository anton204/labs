package lesson10;

public class Lab10 {
    public static void main(String[] args) {
        gazOven oven1 =new gazOven();
        oven1.setOnOrOff(true);
        oven1.food("chicken");
        oven1.temperature(65);
        oven1.setOnOrOff(false);

        elecOven oven2 = new elecOven();
        oven2.setOnOrOff(true);
        oven2.food("pork");
        oven2.temperature(165);
        oven2.setOnOrOff(false);

    }
    static class Oven {
        boolean OnOrOff;
        String whatInside;
        int c;
        public void food(String foodName){
            whatInside=foodName;
        }
        public void setOnOrOff(boolean newOnOrOff) {
            OnOrOff = newOnOrOff;
        }

    }
    static class gazOven extends Oven {
        public void temperature(int newC) {
            c = newC;
        }
    }
    static class elecOven extends Oven {
            public void temperature(int newC) {
                c = newC;
            }
        }
    }
