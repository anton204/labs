package exam;
import java.util.ArrayList;
import java.util.Collections;

public class Final {
    public static void main(String[] args) {
        Tenant tenant1 = new Tenant(Tenant.Driverlicense.D,"Anton");
        Tenant tenant2 = new Tenant(Tenant.Driverlicense.B,"Sergey");
        Tenant tenant3 = new Tenant(Tenant.Driverlicense.A,"James");

        Tractor zhiveBelarus = new Tractor(23,"MTZ","MTZ-80",true,false,false);
        Car honda = new Car(2,"Honda","Accord",true,false, Car.Transmission.AUTOMATE);
        Motorcycle suzuki = new Motorcycle(1,"suzuki","SV400",false);
        Car lada = new Car(28,"Lada","10",true,false, Car.Transmission.HAND);

        ArrayList<Vehicle> isForRent = new ArrayList<>();
        Collections.addAll(isForRent, zhiveBelarus,honda,suzuki,lada);
        System.out.println("Vehicles for rent:");

        tenant1.getavailable(isForRent);

        tenant1.rent(zhiveBelarus);
        tenant2.rent(lada);
        lada.setFree(true);
        tenant2.rent(lada);
        tenant2.rent(honda);
        tenant3.rent(suzuki);

        System.out.println(" Amount of vehicles in our company: " + Vehicle.numberOfVehicles);

    }
}




