package exam;

public class Motorcycle extends Vehicle implements Sharable {
    private boolean isWithSidecar;

    public Motorcycle(int pricePermin, String manufacturer, String model, boolean isWithSidecar) {
        super(pricePermin, manufacturer, model);
        this.isWithSidecar = isWithSidecar;
    }

    public boolean getSidecar() {
        return isWithSidecar;
    }


    public void getOptions() {
        System.out.println((super.isFree() ? "is available" : "") + getManufacturer() + " " + getModel() +
                (getSidecar() ? " with sidecar" : " without sidecar") + " for " + getPricePermin() + "$ per minute");
    }

    @Override
    public void rent(Vehicle vehicle) {
        System.out.println(getManufacturer() + " " + getModel() + (getSidecar() ? "with sidecar" : "without sidecar")
                + " rented for " + getPricePermin() + "$ per minute");
        setFree(false);
    }
}