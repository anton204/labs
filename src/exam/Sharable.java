package exam;

public interface Sharable {
    void rent(Vehicle vehicle);
}