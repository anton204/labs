package exam;

public class Car extends Vehicle implements Sharable {
    private boolean isAirConditionavailable;
    private boolean isHeatedSeatsavailable;

    public enum Transmission {
        HAND,
        AUTOMATE,
        ROBOTIC
    }
    private Transmission transmission;

    public Car(int pricePermin, String manufacturer, String model, boolean isAirConditionavailable, boolean isHeatedSeatsavailable, Transmission transmission) {
        super(pricePermin, manufacturer, model);
        this.isAirConditionavailable = isAirConditionavailable;
        this.isHeatedSeatsavailable = isHeatedSeatsavailable;
        this.transmission = transmission;
    }

    public boolean getAircondition() {
        return isAirConditionavailable;
    }

    public boolean getHeatedSeats() {
        return isHeatedSeatsavailable;
    }

    public Transmission getTransmission() {
        return transmission;
    }



    public void getOptions() {
        System.out.println((super.isFree() ? "is available " : "") + getManufacturer() + " " + getModel()
                + (getAircondition() ? " with air condition " : " without air condition ")
                + (getHeatedSeats() ? " with heated seats " : " without heated seats ")
                + (getTransmission())
                + " for " + getPricePermin() + "$ per minute");
    }

    @Override
    public void rent(Vehicle vehicle) {
        System.out.println(getManufacturer() + " " + getModel() + (getAircondition() ? " with air condition" : " without air condition")
                + (getHeatedSeats() ? " with heated seats " : " without heated seats ")
                + (getTransmission())
                + " rented for " + getPricePermin() + "$ per minute");
        setFree(false);
    }
}