package exam;

import java.util.ArrayList;

public class Tenant implements Sharable {
    public enum Driverlicense {
        A,
        B,
        D
    }

    private Driverlicense license;
    private String name;

    public Tenant(Driverlicense license, String name) {
        this.license = license;
        this.name = name;
    }

    public Driverlicense getLicense() {
        return license;
    }

    public void getavailable(ArrayList<Vehicle> list) {
        for (Vehicle entry : list) {
            if (entry.isFree()) {
                if (getLicense() == Driverlicense.A) {
                    if (entry instanceof Motorcycle) {
                        ((Motorcycle) entry).getOptions();
                    }
                } else if (getLicense() == Driverlicense.B) {
                    if (entry instanceof Car) {
                        ((Car) entry).getOptions();
                    }
                } else if (getLicense() == Driverlicense.D) {
                    if (entry instanceof Tractor) {
                        ((Tractor) entry).getOptions();
                    }
                }
            }
        }
    }

    @Override
    public void rent(Vehicle vehicle) {
        vehicle.setFree(false);
        if (vehicle instanceof Motorcycle) {
            ((Motorcycle) vehicle).rent(vehicle);
        } else if (vehicle instanceof Car) {
            ((Car) vehicle).rent(vehicle);
        } else if (vehicle instanceof Tractor) {
            ((Tractor) vehicle).rent(vehicle);
        }
    }
}
