package exam;

public class Tractor extends Vehicle implements Sharable {

    private boolean isBelorussian;
    private boolean isRed;
    private boolean isWithDriver;

    public Tractor(int pricePermin, String manufacturer, String model, boolean isBelorussian, boolean isRed, boolean isWithDriver) {
        super(pricePermin, manufacturer, model);
        this.isBelorussian = isBelorussian;
        this.isRed = isRed;
        this.isWithDriver = isWithDriver;
    }

    public boolean getBelorussian() {
        return isBelorussian;
    }
    public boolean getRed() {
        return isRed;
    }
    public boolean getDriver() {
        return isWithDriver;
    }

    public void getOptions() {
        System.out.println((super.isFree() ? "is available " : "") + getManufacturer() + " " + getModel()
                + (getBelorussian() ? " belorussian" : " not belorussian")
                + (getRed() ? " red " : " not red ") + (getDriver() ? " with driver " : " without driver ")
                + " for " + getPricePermin() + "$ per minute");
    }

    @Override
    public void rent(Vehicle vehicle) {
        System.out.println(getManufacturer() + " " + getModel() + (getBelorussian() ? " belorussian" : " not belorussian")
                + (getRed() ? " red " : " not red ")
                + (getDriver() ? " with driver " : " without driver ")
                + " rented for "
                + getPricePermin() + "$ per minute");
        setFree(false);
    }
}