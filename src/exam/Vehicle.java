package exam;

abstract class Vehicle implements Sharable {
    private int pricePermin;
    private String manufacturer;
    private String model;
    private boolean isFree = true;
    static int numberOfVehicles;


    public Vehicle(int pricePermin, String manufacturer, String model) {
        this.pricePermin = pricePermin;
        this.manufacturer = manufacturer;
        this.model = model;
        numberOfVehicles+=1;
    }

    public int getPricePermin() {
        return pricePermin;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public void getOptions() {
        System.out.println((isFree() ? "is free  " : "") + getManufacturer() + " " + getModel() + " for " + getPricePermin() + "$ per minute");
    }
}