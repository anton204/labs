package lesson11;


import lesson11.animals.Cat;
import lesson11.animals.Dog;
import lesson11.robot.Terminator;


public class Environment {
    public static void main(String[] args) {
        Cat.sayHello();
        Dog.sayHello();
        Terminator.sayHello();
    }
}
