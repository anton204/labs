package lesson11.Jar;
import lesson11.Products.ForestStaff.Berries;
import lesson11.Products.ForestStaff.Mushrooms;
import lesson11.Products.Wegetables.Cucumbers;
import lesson11.Products.Wegetables.Tomatoes;


public class Jar {
    int tomatoes=Tomatoes.tomatoes;
    int cucumbers=Cucumbers.cucumbers;
    int berries = Berries.berries;
    int mushrooms = Mushrooms.mushrooms;
    public void getOut(int mcucumbers, int mtomatoes, int mberries, int mmushrooms) {
        cucumbers=cucumbers-mcucumbers;
        tomatoes=tomatoes-mtomatoes;
        berries=berries-mberries;
        mushrooms=mushrooms-mmushrooms;
    }

    public void getIn(int mcucumbers, int mtomatoes, int mberries, int mmushrooms) {
        cucumbers=cucumbers+mcucumbers;
        tomatoes=tomatoes+mtomatoes;
        berries=berries+mberries;
        mushrooms=mushrooms+mmushrooms;
    }

    public Jar() {
    }

    public Jar(int cucumbers) {
        this.cucumbers = cucumbers;
    }

    public Jar(int cucumbers, int tomatoes) {
        this.cucumbers = cucumbers;
        this.tomatoes = tomatoes;
    }

    public Jar(int cucumbers, int tomatoes, int berries) {
        this.cucumbers = cucumbers;
        this.tomatoes = tomatoes;
        this.berries = berries;
    }

    public Jar(int cucumbers, int tomatoes, int berries, int mushrooms) {
        this.cucumbers = cucumbers;
        this.tomatoes = tomatoes;
        this.berries = berries;
        this.mushrooms = mushrooms;
    }
}

