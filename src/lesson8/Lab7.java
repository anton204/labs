package lesson8;

import java.util.Scanner;

public class Lab7 {
    public static  void main(String[] args) {
        //first task
        System.out.println("Text your name");
        Scanner scanner = new Scanner(System.in);
        String yourName = scanner.next();
        System.out.println("Hello " + yourName + "!");
        //second task
        System.out.println("Text your string for 2 task");
        Scanner scanner2 = new Scanner(System.in);
        String stroka = scanner2.next();
        System.out.println(stroka.endsWith("ood"));
        //third task
        System.out.println("Text string for 3 th task");
        Scanner scanner3 = new Scanner(System.in);
        String string3 =scanner3.next();
        System.out.println("Text symbol for 3 th task");
        Scanner scanner31 = new Scanner(System.in);
        char string31 =scanner31.next().charAt(0);
        int num1=0;
        for (int i = 0; i < (string3.length()); i++) {
            if ((string3.charAt(i))==string31) {
                num1=num1+1;
            };
        }
        System.out.println(num1);
        //fourth task
        System.out.println("Text number for 4 th task");
        Scanner scanner4 = new Scanner(System.in);
        int num =scanner4.nextInt();
        System.out.println(num + "");
        String numString = Integer.toString(num);
        boolean chetOrNechet = true;
        if ((numString.length())% 2 == 0) {
            chetOrNechet=false;
        }
        System.out.println(chetOrNechet);
        //5 th task
        System.out.println("Text string for 5 th task");
        Scanner scanner5 = new Scanner(System.in);
        String string5 = scanner5.next();
        System.out.println("Text sub string for search");
        Scanner scanner51 = new Scanner(System.in);
        String string51 = scanner51.next();
        string5 = string5.toLowerCase();
        string51 = string51.toLowerCase();
        int num5 =0;
        for (int i=0; i<=string5.length() - string51.length(); i++) {
            String substring=string5.substring(i,i+string51.length()); {
            for (int j=0; substring.equals(string51) && j<1; j++) {
                num5=num5+1;
            }

            }
        }
        System.out.println(num5);
    }
}
