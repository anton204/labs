package lesson19;

public class lab19 {
    public static void main(String[] args) {
        Playable[] instruments={ new Guitar(23), new Drums(23), new Trumpet(22)};
        for (Playable instrument : instruments) {
            instrument.play();
        }
    }


}
interface Playable {
    String KEY = "before major";
    default void play() {
        System.out.println("tonality" + KEY);
    }
}

class Guitar implements Playable {
    private Integer numberOfStrings;

    Guitar(Integer numberOfStrings){
        this.numberOfStrings=numberOfStrings;
    }
    @Override
    public void play() {
        System.out.println("guitar play with number o f " + this.numberOfStrings);
    }
}

class Drums implements Playable {
    private Integer size;

    Drums(Integer size){
        this.size=size;
    }
    @Override
    public void play() {
        Playable.super.play();
        System.out.println("drum play with  " + this.size);
    }
}

class Trumpet implements Playable {
    private Integer diametr;

    Trumpet(Integer diametr){
        this.diametr=diametr;
    }
    @Override
    public void play() {
        System.out.println("Trumpet play with " + this.diametr);
    }
}

class BassGuitar implements Playable {
    public void play() {
        System.out.println("BassGuitar plays ");
    }
}
