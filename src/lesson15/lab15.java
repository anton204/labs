package lesson15;

public class lab15 {
    public static void main(String[] args) {
        //firstTask
        Double doubleWrapper = Double.valueOf(777);
        System.out.println(doubleWrapper);
        String string1 = "1";
        double parseDouble1 = Double.parseDouble(string1);
        System.out.println(parseDouble1);
        byte byte1 = 1;
        Double doubleWrapper1 = new Double(byte1);
        System.out.println(doubleWrapper1);
        short short1 = 2;
        Double doubleWrapper2 = new Double(short1);
        System.out.println(doubleWrapper2);
        char char1 = 3;
        Double doubleWrapper3 = new Double(char1);
        System.out.println(doubleWrapper3);
        int int1 = 4;
        Double doubleWrapper4 = new Double(int1);
        System.out.println(doubleWrapper4);
        long long1 = 5;
        Double doubleWrapper5 = new Double(long1);
        System.out.println(doubleWrapper5);
        float float1 = 6;
        Double doubleWrapper6 = new Double(float1);
        System.out.println(doubleWrapper6);
        String literalTipadoubleToString = Double.toString(float1);
        System.out.println(literalTipadoubleToString);

        int firstNumber = Integer.parseInt("1100001001", 2);
        System.out.println(firstNumber);
        int secondNumber = Integer.parseInt("33332", 5);
        System.out.println(secondNumber);
        int result;
        if (firstNumber > secondNumber) {
            System.out.println("firstNumber is bigger than secondNumber");
            result = firstNumber - secondNumber;
        } else if (firstNumber == secondNumber) {
            System.out.println("firstNumber == secondNumber");
            result = 0;
        } else {
            System.out.println("secondNumber is bigger than firstNumber");
            result = secondNumber - firstNumber;
        }
        System.out.println("difference between higher and lower numbers in binary notation " + (Integer.toString(result, 3)));


        int maxInt = Integer.MAX_VALUE;
        String maxString = Integer.toOctalString(maxInt);
        int howMuchSymbols = 0;
        for (int i = 0; i < maxString.length(); i++) {
            howMuchSymbols = howMuchSymbols + 1;
        }
        System.out.println("there is " + howMuchSymbols + " symbols in this amazing string");
    }
}
