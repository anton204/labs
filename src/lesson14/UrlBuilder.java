package lesson14;
import static lesson14.Constants.*;

public class UrlBuilder {
    private  Url url;
    public UrlBuilder(String hostname) {
        url = new Url(hostname);
    }

    public UrlBuilder setSchema(String schema) {
        this.url.setSchema(schema);
        return this;
    }

    public UrlBuilder setResource(String schema) {
        this.url.setResource(schema);
        return this;
    }

    public String build() {
        return this.url.build();
    }


    class Url {
        private  String schema= "http";
        private  String hostname;
        private  String resource = "";

        public Url(String hostname) {
            this.hostname = hostname;
        }

        public void setSchema(String schema) {
            this.schema = schema;
        }

        public String getSchema() {
            return this.schema;
        }

        public void setResource(String resource) {
            this.resource = resource;
        }

        public  String build() {
            String output = this.getSchema() + "://" + hostname;
            if (resource != "") {
                output = output + "/" + resource;
            }
            return output;
        }
    }
    public static void main(String[] args) {
        String url1 = new UrlBuilder("google.com").setSchema(Schemes.HTTPS).setResource("users").build();
        System.out.println(url1);
        String url2 = new UrlBuilder("t-systems.com").setSchema(Schemes.FTP).setResource("contacts").build();
        System.out.println(url2);
        String url3 = new UrlBuilder("yandex.ru").build();
        System.out.println(url3);
        String url4 = new UrlBuilder("canvas.com").setSchema(Schemes.FTP).setResource("courses").build();
        System.out.println(url4);
    }
}