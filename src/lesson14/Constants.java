package lesson14;

public class Constants {
    public static class Schemes{
        public final static String HTTP = "http";
        public final static String HTTPS = "https";
        public final static String FTP = "ftp";
    }
}
