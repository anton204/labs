package lesson21;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;


public class GameFrame extends JFrame {

    public GameFrame() {

        this.setTitle("Game");
        this.setSize(new Dimension(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT));
        this.setResizable(false);
        this.setFocusable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        GamePanel gamePanel = new GamePanel();
        this.add(gamePanel);
        this.setVisible(true);
        gamePanel.grabFocus();
        this.pack();
    }

}
