package lesson21;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GamePanel extends JPanel implements ActionListener, KeyListener {
    private final Brick[] bricks;
    private final Ball ball;
    private final Paddle paddle;
    private final Timer timer;
    private int remainingBricks = Constants.BRICK_COUNT;
    int GameHighScore;
    boolean qwer;


    public GamePanel() {
        File highScore = new File("src/highScore.txt");
        try {
            System.out.println(highScore.getCanonicalPath() + " file found? " + highScore.exists());
        } catch (IOException e) {
            e.printStackTrace();
        }
        qwer=highScore.exists();
        if (qwer==false) {
            GameHighScore=0;
        }
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT));
        this.addKeyListener(this);
        int brickCounter = 0;
        bricks = new Brick[Constants.BRICK_COUNT];
        for (int i = 0; i < 5; i += 1) {
            for (int j = 0; j < 6; j += 1) {
                bricks[brickCounter] = new Brick(j * 40 + 30, i * 10 + 50);
                brickCounter += 1;
            }
        }
        ball = new Ball();
        paddle = new Paddle();
        timer = new Timer(Constants.TIMER_PERIOD, this);
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        this.drawObjects(g2d);
    }

    void drawObjects(Graphics2D g2d) {
        for (int i = 0; i < Constants.BRICK_COUNT; i += 1) {
            if (!bricks[i].isDestroyed()) {
                g2d.drawImage(bricks[i].getImage(), bricks[i].getX(), bricks[i].getY(), bricks[i].getImageWidth(), bricks[i].getImageHeight(), this);
            }
        }
        g2d.drawImage(ball.getImage(), ball.getX(), ball.getY(), ball.getImageWidth(), ball.getImageHeight(), this);
        g2d.drawImage(paddle.getImage(), paddle.getX(), paddle.getY(), paddle.getImageWidth(), paddle.getImageHeight(), this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.ball.move();
        this.paddle.move();
        try {
            this.detectCollisions();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        paddle.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        paddle.keyReleased(e);
    }

    private void detectCollisions() throws IOException {
        if (ball.getRect().getMaxY() > Constants.SCREEN_BOTTOM) {
            timer.stop();
            System.out.println("Game Over");

        }
        if (ball.getRect().intersects(paddle.getRect())) {
            int paddleXPos = (int) paddle.getRect().getMinX();
            int ballXPos = (int) ball.getRect().getMinX();
            int firstPoint = paddleXPos + 8;
            int secondPoint = paddleXPos + 16;
            int thirdPoint = paddleXPos + 24;
            int fourthPoint = paddleXPos + 32;
            if (ballXPos < firstPoint) {
                ball.setDirectionX(-1);
                ball.setDirectionY(-1);
            }
            if (ballXPos >= firstPoint && ballXPos < secondPoint) {
                ball.setDirectionX(-1);
                ball.setDirectionY(-1 * ball.getDirectionY());
            }
            if (ballXPos >= secondPoint && ballXPos < thirdPoint) {
                ball.setDirectionX(0);
                ball.setDirectionY(-1);
            }
            if (ballXPos >= thirdPoint && ballXPos < fourthPoint) {
                ball.setDirectionX(1);
                ball.setDirectionY(-1 * ball.getDirectionY());
            }
            if (ballXPos >= fourthPoint) {
                ball.setDirectionX(1);
                ball.setDirectionY(-1);
            }
        }
        for (int i = 0; i < Constants.BRICK_COUNT; i += 1) {
            if (ball.getRect().intersects(bricks[i].getRect())) {
                if (!bricks[i].isDestroyed()) {
                    int ballLeft = (int) ball.getRect().getMinX();
                    int ballHeight = (int) ball.getRect().getHeight();
                    int ballWidth = (int) ball.getRect().getWidth();
                    int ballTop = (int) ball.getRect().getMinY();
                    Point pointRight = new Point(ballLeft + ballWidth + 1, ballTop);
                    Point pointLeft = new Point(ballLeft - 1, ballTop);
                    Point pointTop = new Point(ballLeft, ballTop - 1);
                    Point pointBottom = new Point(ballLeft, ballTop + ballHeight + 1);
                    if (bricks[i].getRect().contains(pointRight)) {
                        ball.setDirectionX(-1);
                    } else if (bricks[i].getRect().contains(pointLeft)) {
                        ball.setDirectionX(1);
                    }
                    if (bricks[i].getRect().contains(pointTop)) {
                        ball.setDirectionY(1);
                    } else if (bricks[i].getRect().contains(pointBottom)) {
                        ball.setDirectionX(-1);
                    }
                    bricks[i].setDestroyed(true);

                    this.remainingBricks -=1;
                    if (this.remainingBricks ==0) {
                        System.out.println("you win");
                        timer.stop();
                    }
                    int score = (Constants.BRICK_COUNT - this.remainingBricks) *100;
                    System.out.println("you score: " + score);

                    FileReader reader = new FileReader("src/highScore.txt");
                    BufferedReader reader1 = new BufferedReader (reader);
                    int score1 = 0;
                    score1=Integer.parseInt(reader1.readLine());
                    reader.close();
                    PrintWriter pw = new PrintWriter("src/highScore.txt");
                    if (score > score1) {
                        pw.print(score);
                        System.out.println("congratulate with new record");
                    }
                    pw.flush();
                }
            }
        }
    }
}