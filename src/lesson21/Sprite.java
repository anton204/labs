package lesson21;

import java.awt.*;

public abstract class Sprite {
    private int x;
    private int y;
    private int imageWidth;
    private int imageHeight;
    private Image image;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public Image getImage() {
        return image;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void calculateImageSize() {
        this.imageWidth = image.getWidth(null);
        this.imageHeight = image.getHeight(null);
    }

    public Rectangle getRect() {
        return new Rectangle(x, y, image.getWidth(null), image.getHeight(null));
    }
}

