package lesson3;

public class Lab2 {
    public static void main(String[] args) {
        byte byteValue = 127;
        System.out.println("byte type= "+ byteValue);
        short shortValue = 32767;
        System.out.println("short type= "+ shortValue);
        int intValue = 2147483647;
        System.out.println("int type= "+ intValue);
        long longValue = 12345;
        System.out.println("long type= "+ longValue);
        float floatValue = (float)10/3;
        System.out.println("float type= "+ floatValue % 10);
        double doubleValue = 3.14159;
        System.out.println("double type= "+ doubleValue);
        char charValue = 'a';
        System.out.println("char type= "+ charValue);
        boolean booleanValue = true;
        System.out.println("boolean type= "+ booleanValue);
        String stringValue = ("Lab2 completed");
        System.out.println("string type= " + stringValue);
    }
}