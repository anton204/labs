package Lesson5;

import java.util.Scanner;


public class Lab4 {
    public static void main(String[] args) {
 //first task
        System.out.println("Text 0 if you like winter, 1 - for spring, 2 - for summer, 3-autumn?");
        Scanner scanner1= new Scanner(System.in);
        int result1 = scanner1.nextInt();
        switch (result1) {
            case 0:
                System.out.println("winter");
                break;
            case 1:
                System.out.println("spring");
                break;
            case 2:
                System.out.println("summer");
                break;
            default:
                System.out.println("autumn");
        }
//second task
        int a = 5;
        int b = 22;
        int c = 2434;
        boolean boolean102 = false;
        if ((a<b)&(b<c)) {
            boolean102 = true;
            System.out.println(boolean102);
        } else {
            System.out.println(boolean102);
        }
//third task
        System.out.println("text a number for 3-th task?");
        Scanner scanner103= new Scanner(System.in);
        int result103 = scanner103.nextInt();
        int factorial=1;
        while (result103>0) {
            factorial=factorial*result103;
            result103-=1;
        }
        System.out.println(factorial);
//4-th task
        int reverseNumber=0;
        System.out.println("text a number for 4 th task?");
        Scanner scanner104= new Scanner(System.in);
        int result104 = scanner104.nextInt();
        while (result104!=0) {
            reverseNumber=reverseNumber*10;
            reverseNumber=reverseNumber + result104 % 10;
            result104=result104/10;
        }
        System.out.println(reverseNumber);
//5-th task

        int b105=3;
        int sumNumber=0;
        int i105=0;
        while ((i105<10)&(b105%3==0)) {
             sumNumber=sumNumber+b105;
             b105+=3;
             i105+=1;

        }

        System.out.println(sumNumber/10);
//6 th task
        System.out.println("text a number for 6 th task?");
        Scanner scanner106= new Scanner(System.in);
        long result106 = scanner106.nextLong();
        long numberOfNumbers=0;
        while (result106>=1) {
            result106=result106/10;
            numberOfNumbers+=1;
;
        }
        System.out.println(numberOfNumbers);
//7th tsk
        System.out.println("text a number for 7 th task?");
        Scanner scanner107= new Scanner(System.in);
        int result107 = scanner107.nextInt();
        while (result107 != 1 && result107 % 2 == 0) {
            result107 /= 2;
        }
        System.out.println(result107 == 1 ? "This number is a power of 2" : "This number is not a power of 2");
//8th tsk
        String[] myArray1 = new String [5];
        String[] myArray2 = new String [5];
        myArray1[0] = "Rulon";
        myArray1[1] = "Zabeg";
        myArray1[2] = "Pobeg";
        myArray1[3] = "Razbor";
        myArray1[4] = "Meshok";
        myArray2[0] = "Kovboev";
        myArray2[1] = "Oboev";
        myArray2[2] = "Zlodeev";
        myArray2[3] = "Poletov";
        myArray2[4] = "Limonov";
        long superheroName = Math.round(Math.random() * 4);
        long superheroSurName = Math.round(Math.random() * 4);
        for (int i = 0; i < myArray1.length; i += 1) {
            if (i==superheroName) {
                System.out.print(myArray1[i]+" ");
            }
        }
        for (int i = 0; i < myArray2.length; i += 1) {
            if (i==superheroSurName) {
                System.out.print(myArray2[i]);
            }
        }
    }


}
