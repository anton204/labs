package lesson4;

import java.util.Scanner;

public class Lab3 {
    public static void main(String[] args) {
        //first task
        int A1=2;
        int B1=3;
        int C1=4;
        boolean boolean2=((A1==2)|(B1==222));
        System.out.println(boolean2);
        boolean boolean3=((A1==2)&(B1==222));
        System.out.println(boolean3);
        boolean boolean4=((C1==4)^(B1==222));
        System.out.println(boolean4);
        boolean boolean5=((A1!=2));
        System.out.println(boolean5);
        boolean boolean6=((C1==4)&!(B1==222));
        System.out.println(boolean6);
        boolean boolean7=((A1==4)||!(B1==72));
        System.out.println(boolean7);

        //second task

        System.out.println("Text  number for the second task");
        Scanner scanner10= new Scanner(System.in);
        double A2 = scanner10.nextInt();
        if (A2==0) {
            System.out.println(Double.MAX_VALUE);
        } else {
            System.out.println(((float) A2/100));
        }

        //third task

        System.out.println("Text A number");
        Scanner scanner1= new Scanner(System.in);
        int A = scanner1.nextInt();

        System.out.println("Text B number");
        Scanner scanner2= new Scanner(System.in);
        int B = scanner2.nextInt();

        System.out.println("Text C number");
        Scanner scanner3= new Scanner(System.in);
        int C = scanner3.nextInt();

        System.out.println("Text D number");
        Scanner scanner4= new Scanner(System.in);
        int D = scanner4.nextInt();

        boolean boolean1 = false;
        if (((A%2==0)&(B%4==0))|((C%2==0)&(D%4!=0)))
        {boolean1 =true;}
        System.out.println("A is divisible by 2 and B is divisible by4, or C is divisible by 3 or B is not divisible by4? " + boolean1);

        //fourth task
        System.out.println("text number for 4-th task");
        Scanner scanner444= new Scanner(System.in);
        int A4 = scanner444.nextInt();
        System.out.println(Integer.toBinaryString(A4));
        boolean boolean44 = false;
        System.out.println("I was unable to complete 4-th task");

        //fifth task

        System.out.println("text five-digit number");
        Scanner scanner44= new Scanner(System.in);
        int A5 = scanner44.nextInt();
        System.out.println("which number do you want to see?(from 1 to 5)");
        Scanner scanner67= new Scanner(System.in);
        int D67 = scanner67.nextInt();
        switch (D67) {
            case 5:
                System.out.println(A5/10000);
                break;
            case 4:
                System.out.println((A5/1000)-((A5/10000)*10));
                break;
            case 3:
                System.out.println((A5/100)-((A5/1000)*10));
                break;
            case 2:
                System.out.println((A5/10)-((A5/100)*10));
                break;
            default:
                System.out.println(A5-((A5/10)*10));
        }
    }
}