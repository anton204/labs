package lesson13;

public class lab13 {
    public static void main(String[] args) {
        CaesarCipher cipher = new CaesarCipher();
        String cipheredMessage = cipher.encrypt("i need to make a lot of labs because of vacances", 1);
        System.out.println(cipheredMessage);

        CaesarCipher cipher2 = new CaesarCipher();
        String cipheredMessage2 = cipher2.decrypt("j offe up nblf b mpu pg mbct cfdbvtf pg wbdbodft", 1);
        System.out.println(cipheredMessage2);





        PigCipher cipher1 = new PigCipher();
        String cipheredMessage1 = cipher1.encrypt("qdfdfd wwwww ewwww r t y");
        System.out.println(cipheredMessage1);


    }

    static class PigCipher {

        char[] vowels = {'a', 'e', 'i', 'o', 'u'};

        private boolean isVowel(char vowel) {
            for (char v : vowels) {
                if (vowel == v) {
                    return true;
                }
            }
            return false;
        }

        String encrypt(String source) {
            source = source.toLowerCase();
            char firstChar = source.charAt(0);
            if (this.isVowel(firstChar)) {
                return source + "ay";
            }
            String temp = "";
            char[] spaceHere = {' '};
            for (int i = 0; i < source.length(); i += 1) {
                if (source.charAt(i) == spaceHere[0]) {
                    temp += "ay"+ source.charAt(i);
                } else if (source.charAt(i) != spaceHere[0]) {
                    temp += source.charAt(i);
                } else if ((source.charAt(i) == spaceHere[0]) & (!this.isVowel(source.charAt(i+1)))) {
                    temp += source.charAt(i+1);
                } else {
                    break;
                }
            }
            return temp;

        }

    }


    public static class CaesarCipher {
        String encrypt(String source, int offset) {
            String result = "";
            for (int i = 0; i < source.length(); i += 1) {
                    char currentChar = source.charAt(i);
                    int currentCharPosition = currentChar - 'A';
                    char[] spaceHere = {' '};
                    if (source.charAt(i) != spaceHere[0] ) {
                        int newCharPosition = (currentCharPosition + offset) ;
                        char newChar = (char) ('A' + newCharPosition);
                        result += newChar;
                    } else if (source.charAt(i) == spaceHere[0]) {
                        result += source.charAt(i);
                }
            }
            return result;
        }

        String decrypt(String source, int offset) {
            String result = "";
            for (int i = 0; i < source.length(); i += 1) {
                char currentChar = source.charAt(i);
                int currentCharPosition = currentChar - 'A';
                char[] spaceHere = {' '};
                if (source.charAt(i) != spaceHere[0] ) {
                    int newCharPosition = (currentCharPosition - offset) ;
                    char newChar = (char) ('A' + newCharPosition);
                    result += newChar;
                } else if (source.charAt(i) == spaceHere[0]) {
                    result += source.charAt(i);
                }
            }
            return result;
        }
    }
}



