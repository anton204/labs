package lesson16;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static lesson16.Constants.*;

public class CalculatorPanel extends JPanel {
    JTextField text1 = null;
    String tempValue = null;
    String currentOp = null;

    public CalculatorPanel() {
        setLayout(null);
        JLabel label = new JLabel("Calculator");
        label.setBounds(0,-150,200,200);
        add(label);
        label.setVisible(true);
        label.setSize(400,400);
        JButton b7 = new JButton("7");
        b7.setBounds(0, 200, 50, 50);
        add(b7);
        JButton b8 = new JButton("8");
        b8.setBounds(51, 200, 50, 50);
        add(b8);
        JButton b9 = new JButton("9");
        b9.setBounds(102, 200, 50, 50);
        add(b9);
        JButton b4 = new JButton("4");
        b4.setBounds(0, 251, 50, 50);
        add(b4);
        JButton b5 = new JButton("5");
        b5.setBounds(51, 251, 50, 50);
        add(b5);
        JButton b6 = new JButton("6");
        b6.setBounds(102, 251, 50, 50);
        add(b6);
        JButton b1 = new JButton("1");
        b1.setBounds(0, 302, 50, 50);
        add(b1);
        JButton b2 = new JButton("2");
        b2.setBounds(51, 302, 50, 50);
        add(b2);
        JButton b3 = new JButton("3");
        b3.setBounds(102, 302, 50, 50);
        add(b3);
        JButton b0 = new JButton("0");
        b0.setBounds(51, 352, 50, 50);
        add(b0);
        JButton bPlus = new JButton(Operations.PLUS);
        bPlus.setBounds(155, 200, 50, 50);
        add(bPlus);
        JButton bEq = new JButton("=");
        bEq.setBounds(155, 251, 50, 50);
        add(bEq);

        JButton bMinus = new JButton(Operations.MINUS);
        bMinus.setBounds(155, 301, 50, 50);
        add(bMinus);
        JButton bMult = new JButton(Operations.MULTIPLICATION);
        bMult.setBounds(155, 351, 50, 50);
        add(bMult);

        JButton bDiv = new JButton(Operations.DIVISION);
        bDiv.setBounds(102, 352, 50, 50);
        add(bDiv);




        text1 = new JTextField();
        text1.setBounds(0, 100, 260, 90);
        add(text1);
        ActionListener listenerDigits = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JButton clickedButton = (JButton) e.getSource();
                String buttonLabel = clickedButton.getText();
                text1.setText(text1.getText() + buttonLabel);
            }
        };
        ActionListener listenerEq = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (tempValue != null && text1.getText().length() > 0) {
                    float value1 = Float.valueOf(tempValue);
                    float value2 = Float.valueOf(text1.getText());
                    float result = 0;

                    switch (currentOp) {
                        case Operations.PLUS:
                            result = value1 + value2;
                            break;
                        case Operations.MINUS:
                            result = value1 - value2;
                            break;
                        case Operations.MULTIPLICATION:
                            result = value1 * value2;
                            break;
                        case Operations.DIVISION:
                            if (value2 == 0) {
                                text1.setText("");
                                return;
                            } else {
                                result = value1 / value2;
                            }
                    }
                    text1.setText(String.valueOf(result));
                    tempValue = null;
                    currentOp = null;
                }
            }
        };

        ActionListener listenerA = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JButton clickedButton = (JButton) e.getSource();
                String buttonLabel = clickedButton.getText();
                switch (buttonLabel) {
                    case Operations.PLUS:
                        tempValue = text1.getText();
                        text1.setText("");
                        currentOp = Operations.PLUS;
                        break;
                    case Operations.MINUS:
                        tempValue = text1.getText();
                        text1.setText("");
                        currentOp = Operations.MINUS;
                        break;
                    case Operations.DIVISION:
                        tempValue = text1.getText();
                        text1.setText("");
                        currentOp = Operations.DIVISION;
                        break;
                    case Operations.MULTIPLICATION:
                        tempValue = text1.getText();
                        text1.setText("");
                        currentOp = Operations.MULTIPLICATION;
                }
            }
        };
        b7.addActionListener(listenerDigits);
        b8.addActionListener(listenerDigits);
        b9.addActionListener(listenerDigits);
        b4.addActionListener(listenerDigits);
        b5.addActionListener(listenerDigits);
        b6.addActionListener(listenerDigits);
        b3.addActionListener(listenerDigits);
        b2.addActionListener(listenerDigits);
        b1.addActionListener(listenerDigits);
        b0.addActionListener(listenerDigits);
        bPlus.addActionListener(listenerA);
        bEq.addActionListener(listenerEq);
        bMinus.addActionListener(listenerA);
        bMult.addActionListener(listenerA);
        bDiv.addActionListener(listenerA);
    }
}