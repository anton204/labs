package lesson16;

public class Constants {
    public static class Operations{
        public final static String PLUS = "+";
        public final static String MINUS = "-";
        public final static String MULTIPLICATION = "*";
        public final static String DIVISION = "/";
    }
}
