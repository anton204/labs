package lesson16;

import javax.swing.*;
import java.awt.*;

public class AppGui extends JFrame {
//    public static void main(String[] args) {
//        JFrame frame1 = new JFrame("calc");
//        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame1.setPreferredSize(new Dimension(400, 600));
//        JLabel label1 = new JLabel("Test");
//        frame1.getContentPane().add(label1);
//        frame1.pack();
//        frame1.setVisible(true);
//    }
    public AppGui() {
        setBounds(200,200,260,500);
        setTitle("calc");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new CalculatorPanel());
        setVisible(true);
    }
}
