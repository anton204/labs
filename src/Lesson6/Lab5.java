package Lesson6;

import java.util.Arrays;
import java.util.Random;

public class Lab5 {
    public static void main(String[] args) {
//first task
        int[] myArray501 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("Array before reverse" + Arrays.toString(myArray501));
        for (int i = 0; i < ((myArray501.length) / 2); i++) {
            int num = myArray501[i];
            myArray501[i] = myArray501[(myArray501.length) - 1 - i];
            myArray501[(myArray501.length) - 1 - i] = num;
        }
        System.out.println("Array after reverse" + Arrays.toString(myArray501));
//second task
        Random random = new Random();

        int[][] myArray502 = new int[5][5];
        for (int i = 0; i < myArray502.length; i++) {
            for (int j = 0; j < myArray502[i].length; j++) {
                myArray502[i][j] = (random.nextInt());
            }
        }
        System.out.println("my random array" + (Arrays.deepToString(myArray502)));

        int sumNumbers = 0;
        for (int i = 0; i < myArray502.length; i = i + 1) {
            for (int j = 0; j < myArray502[i].length; j = j+ 1) {
                if (((myArray502[i][j] % 2) == 0) & ((myArray502[i][j]) >0)) {
                    sumNumbers = sumNumbers + myArray502[i][j];
                }
            }
        }
        System.out.println(sumNumbers);
    }
}
