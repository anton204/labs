package lesson18;

import java.util.Date;

class lab18 {
    public static void main(String[] args) {
        Report report1 = new trueReport("Text of the report");
        report1.getReport();


        Report report2= new TimestampedReport("This is text for my TimestampedReport");
        report2.getReport();

        Report report3 = new UsernameReport("This is text for my UsernameReport");
        report3.getReport();

        Reporter.report(report1);
        Reporter.report(report2);
    }
}
    abstract class Report {
        public int numberOfReport;
        public String textOfReport1;


        public void getReport() {
            int numberOfReportISaved;
            numberOfReportISaved = this.numberOfReport;
            System.out.println("My current report: " + numberOfReportISaved + " " + this.textOfReport1);
        }
}

    class trueReport extends Report {


        public trueReport(String textOfReport) {
            super();
            textOfReport1 = textOfReport;
            numberOfReport = numberOfReport + 1;
        }
}
    class TimestampedReport extends trueReport {
        public TimestampedReport(String textOfReport) {
            super(textOfReport);
            textOfReport1 = textOfReport;
            numberOfReport = numberOfReport + 1;
        }

        public void getReport() {
            int numberOfReportISaved;
            Date currentDate = new Date(System.currentTimeMillis());
            numberOfReportISaved = numberOfReport;
            System.out.println("My current report: " + numberOfReportISaved + " " + currentDate + " " + textOfReport1);
        }
}

        class UsernameReport extends TimestampedReport {
            public UsernameReport(String textOfReport) {
                super(textOfReport);
                textOfReport1 = textOfReport;
                numberOfReport = numberOfReport + 1;
            }

            public  void getReport() {
                int numberOfReportISaved;
                String userName = (System.getProperty("user.name"));

                numberOfReportISaved = numberOfReport;
                System.out.println("My current report: " + numberOfReportISaved + " " + userName + " " + textOfReport1);
            }
}

        class Reporter  {
            public static void report(Report reportForReport) {
                reportForReport.getReport();
            }
}




