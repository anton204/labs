package lesson17;
import java.util.*;
import java.text.*;

public class lab17 {
    public static void main(String[] args) {
        truck truck1= new truck();
        truck1.setBodyVolume(25);
        truck1.addVolumeToTheTruck(2);
        truck1.dropVolumeToTheTruck(34);
        truck1.dropVolumeToTheTruck(1);

        passengerCar passengerCar1=new passengerCar();
        passengerCar1.setNumberOfSeats(5);
        passengerCar1.weight=1234;
        passengerCar1.enginePower=123;
        passengerCar1.addPassengers(2);
        passengerCar1.dropPassengers(342);
        passengerCar1.dropPassengers(1);

        dumpTruck dumpTruck1=new dumpTruck();
        dumpTruck1.setBodyVolume(123);
        dumpTruck1.liftTruckBody();
        dumpTruck1.lowerTruckBody();
    }


    static class car {
        int weight;
        int enginePower;
    }

    static class truck extends car {
        static int truckBodyVolume;
        static int volumeOfCargoInTruck;

        public void setBodyVolume (int bodyVolume) {
            truckBodyVolume=bodyVolume;
        }

        public void addVolumeToTheTruck(int howMuchtoAdd){
            if ((howMuchtoAdd+volumeOfCargoInTruck>=truckBodyVolume) & (truckBodyVolume!=0)) {
                System.out.println("truck is full");
                return;
            } else if (truckBodyVolume == 0) {
                System.out.println("Plese set volume of your truck");
                return;
            } else {
                volumeOfCargoInTruck = volumeOfCargoInTruck + howMuchtoAdd;
                System.out.println("volume added");
            }
        }
        public void dropVolumeToTheTruck(int howMuchtoAdd){
            if ((volumeOfCargoInTruck-howMuchtoAdd<=0) & (truckBodyVolume!=0)) {
                System.out.println("truck do not have enough volume or empty");
                return;
            } else if (truckBodyVolume == 0) {
                System.out.println("Plese set volume of your truck");
                return;
            } else {
                volumeOfCargoInTruck = volumeOfCargoInTruck + howMuchtoAdd;
                System.out.println("volume dropped");
            }
        }
    }
    static class dumpTruck extends truck {
        public static void liftTruckBody() {
            System.out.println("body lifted");
        }

        public static void lowerTruckBody() {
            System.out.println("body lowered");
            volumeOfCargoInTruck=volumeOfCargoInTruck*0;
        }
    }

    static class passengerCar extends car {
        static int numberOfSeats;
        static int passengersInCar;

        public void setNumberOfSeats (int howMuchToAdd) {
            numberOfSeats=howMuchToAdd;
        }



        public void dropPassengers(int numberOfPassengers) {
            if ((passengersInCar - numberOfPassengers  <= 0) & (numberOfSeats>0)) {
                System.out.println("car do not have enough passangers or empty");
                return;
            } else if(numberOfSeats==0) {
                System.out.println("Plese set number of seats of your car");
                return;
            } else {
                passengersInCar = passengersInCar - numberOfPassengers;
                System.out.println("passenger dropped");
            }
        }


        public void addPassengers(int numberOfPassengers) {
            if ((numberOfPassengers + passengersInCar >= numberOfSeats) & (numberOfSeats>0)) {
                System.out.println("car is full");
                return;
            } else if(numberOfSeats==0) {
                System.out.println("Plese set number of seats of your car");
                return;
            } else {
                passengersInCar = passengersInCar + numberOfPassengers;
                System.out.println("passenger added");
            }
        }
    }
}