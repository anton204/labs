package lesson7;

import java.util.Arrays;
import java.util.Scanner;

public class Lab6 {
    public static int whoWins(int userChoice, int opponentValue) {
        int max=0;

        if (userChoice==opponentValue) {
            max = 0;}
        else if ((userChoice == 1)&(opponentValue == 0))
        {max = 1; }
        else if (((userChoice == 0)&(opponentValue == 2))) {
            max=2;
        } else if (((userChoice == 2)&(opponentValue == 0))) {
            max=1;
        } else if (((userChoice == 0)&(opponentValue == 3))) {
            max=1;
        } else if (((userChoice == 3)&(opponentValue == 0))) {
            max=2;
        } else if (((userChoice == 0)&(opponentValue == 4))) {
            max=2;
        } else if (((userChoice == 4)&(opponentValue == 0))) {
            max=1;
        } else if (((userChoice == 1)&(opponentValue == 2))) {
            max=1;
        } else if (((userChoice == 2)&(opponentValue == 1))) {
            max=2;
        } else if (((userChoice == 1)&(opponentValue == 3))) {
            max=1;
        } else if (((userChoice == 3)&(opponentValue == 1))) {
            max=2;
        } else if (((userChoice == 1)&(opponentValue == 4))) {
            max=2;
        } else if (((userChoice == 4)&(opponentValue == 1))) {
            max=1;
        } else if (((userChoice == 2)&(opponentValue == 3))) {
            max=2;
        } else if (((userChoice == 3)&(opponentValue == 2))) {
            max=1;
        } else if (((userChoice == 3)&(opponentValue == 4))) {
            max=1;
        } else if (((userChoice == 4)&(opponentValue == 3))) {
            max=2;
        } else if (((userChoice == 2)&(opponentValue == 4))) {
            max=1;
        } else if (((userChoice == 4)&(opponentValue == 2))) {
            max=2;
        }

        return max;
    }
    public static void game() {
        System.out.println("Text your choice: 0 - rock; 1- scissors; 2-paper, 3-lizard, 4-spock, 9 for exit");
        Scanner scanner = new Scanner(System.in);
        int userChoice = scanner.nextInt();
        if (userChoice==9) {
            System.out.println("Ok,Exit" );
            System.exit(0);
        }
        int opponentValue = (int) Math.round((Math.random() * 5)-1);
        String[] myArray1 = new String [5];
        myArray1[0] = "Rock";
        myArray1[1] = "Scissors";
        myArray1[2] = "Paper";
        myArray1[3] = "Lizard";
        myArray1[4] = "Spock";
        for (int i = 0; i < myArray1.length; i += 1) {
            if (userChoice==(i)) {
                System.out.println("User choice: " + myArray1[i]);
            }
        }
        for (int i = 0; i < myArray1.length; i += 1) {
            if (opponentValue==(i)) {
                System.out.println("Computer choice: " + myArray1[i]);
            }
        }
        int result=whoWins(userChoice,opponentValue);
        if (result==0) {
            System.out.println("Draw");
        } else if (result==1) {
            System.out.println("User Wins");
        } else if (result==2) {
            System.out.println("Computer Wins");
        }

        game();
    }

        public static  void main(String[] args) {
        game();

    }
}