package lesson12;

public class lab12 {
     static class Address{
        protected String country;
        public String town;
        public String street;
        protected int building;
        public int zip;

        Address() {}
        Address (String country) {
            this.country=country;
        }
        Address (String country, String town) {
            this.country=country;
            this.town=town;
        }
        Address (String country, String town, String street) {
            this.country=country;
            this.town=town;
            this.street=street;
        }
        Address (String country, String town, String street, int building) {
            this.country=country;
            this.town=town;
            this.street=street;
            this.building=building;
        }
        Address (String country, String town, String street, int building, int zip) {
            this.country=country;
            this.town=town;
            this.street=street;
            this.building=building;
            this.zip=zip;
        }


    }
    static class Person extends Address {
        protected String name;
        int age;
        public String Address125;

        public String fullAddress(){
            Address Address123 = new Address("Russia","Tikhvin","ssdsad");
            String Address125 = Address123.toString();
            return Address125;
        }

        Person() {}
        Person (String name) {
            this.name=name;
        }
        Person (String name, int age) {
            this.name=name;
            this.age=age;
        }
        Person (String name, int age, String Address125) {
            this.name=name;
            this.age=age;
            this.Address125=Address125;
        }
        public String toString(){
            return this.name + this.age + this.Address125 ;
        }

    }
    public static  void main(String[] args) {

        Person Person1 = new Person("Anton",24, "Russia");
        Person Person2 = new Person();
        Person Person3 = new Person("Sergey");
        System.out.println(Person1.toString());
        System.out.println(Person2.toString());
        System.out.println(Person3.toString());
    }
}